local lib = PezLib:NewLibrary("PezConsole");

-- If lib is nil, then the library must already be loaded
if (not lib) then
	return;
end


-- Cache globals
local LibStub = LibStub;
local ChatFrame3 = ChatFrame3;
local error = error;
local format = format;
local pairs = pairs;
local select = select;


-- Load required libs
local AceConsole = LibStub("AceConsole-3.0");
local PezUtil = LibStub("PezUtil");
local PezClass = LibStub("PezClass");
local PezColor = LibStub("PezColor");


-- Define local constants
local DEBUG_CHAT_FRAME = ChatFrame3;
local PRINT_COLOR = PezColor.WHITE;
local ERROR_COLOR = PezColor.ERROR;
local DEBUG_COLOR = PezColor.QUALITY.LEGENDARY;
local PRINT_FUNCTIONS = {
	"Print",
	"Printf",
	"Debug",
	"Debugf",
	"Error",
	"Errorf",
	"Throw",
	"Throwf"
};


------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Module class definition
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

-- Create class
lib = PezClass:Create(lib);

--- Class constructor
function lib:__constructor(addon, label)
	self.addon = addon;
	self.label = label;
end



--============================================================================--
-- Local functions
--============================================================================--

--- Prints to the default chat frame via AceConsole. If debug mode is turned on, the print will be
-- duplicated in the debug chat frame.
-- @param addon (table) The addon from which the print will be generated
-- @param color (table) Color table specifying the color of the printed text
-- @param ... (*) List of values to print
local function print(console, color, ...)
	local addon = console.addon;

	-- Construct prefix with addon title
	local label = console.label and (color(" : ") .. console.label) or "";
	local prefix = color("<") .. addon.title .. label .. color(">", false);

	-- Print to the default chat frame
	AceConsole:Print(prefix, ...);

	-- In debug mode, also print to the debug chat frame
	if (addon:IsInitialized() and addon.db.debug) then
		AceConsole:Print(DEBUG_CHAT_FRAME, prefix, ...);
	end
end



--============================================================================--
-- Library functions
--============================================================================--

--- Embeds an alias to each print function in a given table.
-- @param target (table) Table to embed the aliases in
function lib:Embed(target)
	local console = self;

	-- For each print function...
	for _, name in pairs(PRINT_FUNCTIONS) do

		-- Store a function in the target that calls the print function on this console
		target[name] = function(...)
			console[name](console, select(2, ...));
		end
	end
end

--- Prints a message to the chat frame.
-- @param ... (string) List of strings to print
function lib:Print(...)
	print(self, PRINT_COLOR, ...);
end

--- Prints a formatted message to the chat frame.
-- @param formatString (string) Format string
-- @param ... (*) List of variable replacements
function lib:Printf(formatString, ...)
	self:Print(format(formatString, ...));
end

--- Prints a debug message to the chat frame if, and only if, debug mode is turned on.
-- @param ... (string) List of strings to print
function lib:Debug(...)

	-- DB must be initialized before Debug() can be called,
	-- so provide a more informative error when this occurs
	if (not self.addon:IsInitialized()) then
		return self:Throw("Cannot call 'Debug()' before addon is initialized");
	end

	-- If in debug mode, print debug message
	if (self.addon.db.debug) then
		print(self, DEBUG_COLOR, ...);
	end
end

--- Prints a formatted message to the chat frame if, and only if, debug mode is turned on.
-- @param formatString (string) Format string
-- @param ... (*) List of variable replacements
function lib:Debugf(formatString, ...)
	self:Debug(format(formatString, ...));
end

--- Prints an error message to the chat frame.
-- @param ... (string) List of strings to print
function lib:Error(...)
	print(self, ERROR_COLOR, ...);
end

--- Prints a formatted error message to the chat frame.
-- @param formatString (string) Format string
-- @param ... (*) List of variable replacements
function lib:Errorf(formatString, ...)
	self:Error(format(formatString, ...));
end

--- Prints an error message to the chat frame and throws a system error.
-- @param ... (string) List of strings to print
function lib:Throw(...)
	self:Error(...);
	error(PezUtil.concat(..., " "));
end

--- Prints a formatted error message to the chat frame and throws a system error.
-- @param formatString (string) Format string
-- @param ... (*) List of variable replacements
function lib:Throwf(formatString, ...)
	self:Throw(format(formatString, ...));
end