local lib = PezLib:NewLibrary("PezColor");

-- If lib is nil, then the library must already be loaded
if (not lib) then
	return;
end


-- Cache globals
local _G = _G;
local pairs = pairs;
local format = format;
local tostring = tostring;
local print = print;
local string = string;


-- Load required libs
local PezUtil = LibStub("PezUtil");
local PezClass = LibStub("PezClass");



------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Color class definition
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

-- Create class
PezClass:Create(lib);

--- Determines whether a given set of color components represents a valid color.
-- @param red (number) Red color component in the range [0, 1]
-- @param green (number) Green color component in the range [0, 1]
-- @param blue (number) Blue color component in the range [0, 1]
-- @param alpha (number) Alpha color component in the range [0, 1]
-- @return (boolean) True if 'red', 'green', and 'blue' are valid color component values and 'alpha' is
-- either a valid color value or nil
local function validRGBA(red, green, blue, alpha)
	return
	PezUtil.isNumber(red) and PezUtil.inRange(red, 0, 1) and
		PezUtil.isNumber(green) and PezUtil.inRange(green, 0, 1) and
		PezUtil.isNumber(blue) and PezUtil.inRange(blue, 0, 1) and
		(PezUtil.isNumber(alpha) and PezUtil.inRange(alpha, 0, 1) or PezUtil.isNil(alpha));
end

--- Unpacks a given color table into a list of red, green, blue, and alpha components.
-- The provided color table should be keyed in one of the following ways:
--      { 1.0, 1.0, 1.0, 1.0 }
--      { r = 1.0, g = 1.0, b = 1.0, a = 1.0 }
--      { red = 1.0, green = 1.0, blue = 1.0, alpha = 1.0 }
-- @param color A color table
-- @return (number) Red color component
-- @return (number) Green color component
-- @return (number) Blue color component
-- @return (number) Alpha color component
function lib.unpackColorTable(color)
	if (not PezUtil.isTable(color)) then
		error("Invalid color table");
	end

	if (validRGBA(color["red"], color["green"], color["blue"], color["alpha"])) then
		return color["red"], color["green"], color["blue"], color["alpha"];
	elseif (validRGBA(color["r"], color["g"], color["b"], color["a"])) then
		return color["r"], color["g"], color["b"], color["a"];
	elseif (validRGBA(color[1], color[2], color[3], color[4])) then
		return color[1], color[2], color[3], color[4];
	end

	error("Invalid color table");
end

--- Class constructor.
-- @param red (number) Red color component in the range [0, 1]
-- @param green (number) Green color component in the range [0, 1]
-- @param blue (number) Blue color component in the range [0, 1]
-- @param alpha (number) Alpha color component in the range [0, 1]
function lib:__constructor(color) end
function lib:__constructor(red, green, blue, alpha)

	-- If color table provided, unpack it
	if (PezUtil.isTable(red)) then
		red, green, blue, alpha = lib.unpackColorTable(red);
	end

	self.red = red;
	self.green = green;
	self.blue = blue;
	self.alpha = alpha or 1.0;
end



--============================================================================--
-- Accessors
--============================================================================--

--- Returns the color's RGB values.
-- @return {number) Red color component in the range [0, 1]
-- @return {number) Green color component in the range [0, 1]
-- @return {number) Blue color component in the range [0, 1]
function lib:GetRGB()
	return self.red, self.green, self.blue;
end

--- Returns the color's RGBA values.
-- @return {number) Red color component in the range [0, 1]
-- @return {number) Green color component in the range [0, 1]
-- @return {number) Blue color component in the range [0, 1]
-- @return {number) Alpha color component in the range [0, 1]
function lib:GetRGBA()
	return self:GetRGB(), self.alpha;
end

--- Returns the color's RGB values as byte values in the range [0, 255].
-- @return {number) Red color byte in the range [0, 255]
-- @return {number) Green color byte in the range [0, 255]
-- @return {number) Blue color byte in the range [0, 255]
function lib:GetRGBAsBytes()
	return (self.red * 255), (self.green * 255), (self.blue * 255);
end

--- Returns the color's RGBA values as byte values in the range [0, 255].
-- @return {number) Red color byte in the range [0, 255]
-- @return {number) Green color byte in the range [0, 255]
-- @return {number) Blue color byte in the range [0, 255]
-- @return {number) Alpha color byte in the range [0, 255)
function lib:GetRGBAAsBytes()
	return (self.red * 255), (self.green * 255), (self.blue * 255), (self.alpha * 255);
end

--- Generates a hex color string representing the color.
-- @return (string) Hexadecimal color string
function lib:ToHex()
	local red, green, blue, alpha = self:GetRGBAAsBytes();
	return format("%.2X%.2X%.2X%.2X", alpha, red, green, blue);
end

--- Generates an indexed color table representing the color.
-- @return (table) Indexed color table
function lib:ToColorTable()
	return { self.red, self.green, self.blue, self.alpha };
end



--============================================================================--
-- Other functions
--============================================================================--

--- Constructs a colorized version of a given string.
-- @param text (string) Text to be colored
-- @param reset (boolean) Whether to place a color reset escape at the end of the string (defaults to true)
-- @return (string) The colorized string
function lib:Colorize(text, reset)

	-- Default reset to true
	reset = reset or PezUtil.isNil(reset);

	-- Generate hex color string, construct colorized string, and return
	return format("|c%s%s%s", self:ToHex(), tostring(text), (reset and "|r" or ""));
end

--- Alias call function to 'Colorize()'
lib.__call = lib.Colorize;

--- Prints the name of each color constant in the color it represents, for reference purposes.
function lib:ListColors(tbl, path)
	tbl = tbl or self;

	-- For every element of the table...
	for key, value in pairs(tbl) do
		local path = path and (path .. "." .. key) or key;

		-- If the value is a table and the key is in all caps (constant name convention)
		if (PezUtil.isTable(value) and string.match(key, "^[%u_]+$")) then

			-- If the value is a color instance, print it
			if (lib:IsInstance(value)) then
				print(format("%s: #%s", value(key), value:ToHex()));
			end

			-- Otherwise, search it for other colors
			self:ListColors(value, path);
		end
	end
end



------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Color constants
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

-- Class colors (SharedXML/Util.lua)
lib.CLASS = {};
for name, color in pairs(_G.RAID_CLASS_COLORS) do
	lib.CLASS[name] = lib:New(color);
end


-- Quality colors (FrameXML/UIParent.lua)
-- Yes, these are indexed starting from zero (the infinite wisdom of Blizzard devs)
lib.QUALITY = {};
for i=0, _G.NUM_LE_ITEM_QUALITYS do
	lib.QUALITY[i] = lib:New(_G.ITEM_QUALITY_COLORS[i]);
end


-- Alias quality colors by name
lib.QUALITY.POOR        = lib.QUALITY[0];
lib.QUALITY.COMMON      = lib.QUALITY[1];
lib.QUALITY.UNCOMMON    = lib.QUALITY[2];
lib.QUALITY.RARE        = lib.QUALITY[3];
lib.QUALITY.EPIC        = lib.QUALITY[4];
lib.QUALITY.LEGENDARY   = lib.QUALITY[5];
lib.QUALITY.ARTIFACT    = lib.QUALITY[6];
lib.QUALITY.HEIRLOOM    = lib.QUALITY[7];
lib.QUALITY.WOW_TOKEN   = lib.QUALITY[8];


-- System font colors
lib.FONT = {};
lib.FONT.NORMAL         = lib:New(_G.NORMAL_FONT_COLOR);
lib.FONT.HIGHLIGHT      = lib:New(_G.HIGHLIGHT_FONT_COLOR);
lib.FONT.RED            = lib:New(_G.RED_FONT_COLOR);
lib.FONT.DIM_RED        = lib:New(_G.DIM_RED_FONT_COLOR);
lib.FONT.GREEN          = lib:New(_G.GREEN_FONT_COLOR);
lib.FONT.GRAY           = lib:New(_G.GRAY_FONT_COLOR);
lib.FONT.YELLOW         = lib:New(_G.YELLOW_FONT_COLOR);
lib.FONT.LIGHTYELLOW    = lib:New(_G.LIGHTYELLOW_FONT_COLOR);
lib.FONT.ORANGE         = lib:New(_G.ORANGE_FONT_COLOR);
lib.FONT.PASSIVE_SPELL  = lib:New(_G.PASSIVE_SPELL_FONT_COLOR);
lib.FONT.BATTLENET      = lib:New(_G.BATTLENET_FONT_COLOR);
lib.FONT.TRANSMOGRIFY   = lib:New(_G.TRANSMOGRIFY_FONT_COLOR);


-- Coommon colors
lib.WHITE   = lib:New(1.0, 1.0, 1.0);
lib.BLACK   = lib:New(0.0, 0.0, 0.0);
lib.ERROR   = lib:New(0.78, 0.25, 0.25);
lib.ON      = lib:New(0.0, 0.8, 0.4);
lib.OFF     = lib:New(1.0, 0.2, 0.2);
lib.GOOD    = lib.ON;
lib.BAD     = lib.OFF;
lib.ELV     = lib:New(0.09, 0.51, 0.82);
lib.MODULE  = lib.CLASS.SHAMAN;