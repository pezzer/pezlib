local lib = PezLib:NewLibrary("PezUtil");

-- If lib is nil, then the library must already be loaded
if (not lib) then
	return;
end


-- Cache globals
local type = type;
local table = table;
local string = string;
local select = select;
local pairs = pairs;
local setmetatable = setmetatable;
local UIParent = UIParent;
local DevTools_Dump = DevTools_Dump;



--============================================================================--
-- Tables
--============================================================================--

--- Constructs a list of keys found in a given table.
-- @param tbl (table) Table for which keyset will be constructed
-- @return (table) List of keys found in the given table
-- @return (number) Number of keys found in the given table
function lib.keyset(tbl)
	local keyset = {};
	local count = 0;

	-- Loop through each value, storing the key and incrementing the count
	for key, _ in pairs(tbl) do
		count = count + 1;
		keyset[count] = key;
	end

	-- return keyset and count
	return keyset, count;
end

--- Pushes values onto the front of a given table.
-- @param tbl (table) Table to push the value onto
-- @param ... (*) Values to be pushed (in order) onto the table
-- @return (table) The table, for convenience
function lib.push(tbl, ...)
	for i = 1, select("#", ...) do
		table.insert(tbl, 1, select(i, ...));
	end
end

--- An alias of push.
-- @param tbl (table) Table to push the value onto
-- @param value (*) Value to be pushed onto the table
-- @return (table) The table, for convenience
lib.prepend = lib.push;

--- Pushes values onto the front of a given table.
-- @param tbl (table) Table to push the value onto
-- @param values (table) Values to be pushed (in order) onto the table
-- @return (table) The table, for convenience
function lib.pushAll(tbl, values)
	for i = 1, #values do
		table.insert(tbl, 1, values[i]);
	end
	return tbl;
end

--- An alias of pushAll.
-- @param tbl (table) Table to push the value onto
-- @param values (table) Values to be pushed (in order) onto the table
-- @return (table) The table, for convenience
lib.prependAll = lib.pushAll;

--- Pops the first value off of a given table.
-- @param tbl (table) Table to pop off of
-- @return (*) The value popped off of the table
function lib.pop(tbl)
	return table.remove(tbl, 1);
end

--- An alias of pop.
-- @param tbl (table) Table to pop off of
-- @return (*) The value popped off of the table
lib.dequeue = lib.pop;

--- Appends values to the end of a given table.
-- @param tbl (table) Table to be appended to
-- @param ... (*) Values to be appended (in order) to the table
-- @return (table) The table, for convenience
function lib.queue(tbl, ...)
	for i = 1, select("#", ...) do
		tbl[#tbl + 1] = select(i, ...);
	end
end

--- An alias of queue.
-- @param tbl (table) Table to be appended to
-- @param value (*) Value to be appended to the table
-- @return (table) The table, for convenience
lib.append = lib.queue;

--- Appends values to the end of a given table.
-- @param tbl (table) Table to be appended to
-- @param values (table) Values to be appended (in order) to the table
-- @return (table) The table, for convenience
function lib.queueAll(tbl, values)
	for i = 1, #values do
		tbl[#tbl + 1] = values[i];
	end
	return tbl;
end

--- An alias of queueAll.
-- @param tbl (table) Table to be appended to
-- @param values (table) Values to be appended (in order) to the table
-- @return (table) The table, for convenience
lib.appendAll = lib.queueAll;

--- Performs a merge of one table into another, overwriting duplicate entries.
-- @param t1 (table) The table being merged into. All values in t2 will be set
-- in t1
-- @param t2 (table) The table who's values will be merged into t1
-- @param deep (boolean) True for deep copy; false otherwise (defaults to true)
function lib.merge(t1, t2, deep)

	-- Default deep to true
	deep = deep or lib.isNil(deep);

	-- Loop through all elements in 't2'
	-- If the element is a table, and deep merge is enabled, recursively merge
	-- Otherwise, copy the value into 't1'
	for key, value in pairs(t2) do
		if (deep and lib.isTable(t1[key]) and lib.isTable(value)) then
			lib.merge(t1[key], value, true);
		else
			t1[key] = value;
		end
	end
end

--- Maps a given function to all elements of a given table. The function will be
-- passed the value, key and whatever additional parameters are provided.
-- @param tbl (table) Table to iterate
-- @param func (function) Function to be called for each element in the table
-- @param ... (*) Additional parameters to pass to the mapped function
function lib.map(tbl, func, ...)
	for key, value in pairs(tbl) do
		func(value, key, ...);
	end
end



--============================================================================--
-- Lists
--============================================================================--

--- Counts the number of arguments passed to it.
-- @param ... (*) Any number of arguments
-- @return (number) The number of arguments passed in
function lib.argCount(...)
	return select("#", ...);
end


--============================================================================--
-- Strings
--============================================================================--

--- Splits a given string by a given separator character. If no separator is provided, the string will be split on " ".
-- @param str (string) The string to split
-- @param separator (string) separator to split on (defaults to " ")
function lib.split(str, separator)
	local separator = separator or " ";
	local words = {};

	local pattern = string.format("([^%s]+)", separator);
	str:gsub(pattern, function(word)
		lib.append(words, word);
	end);

	return words;
end

--- Concatenates a list of strings.
-- @param ... (string) Strings to be concatenated
-- @param delimiter (string) Delimiter string
function lib.concat(...)
	local list = {...};
	local delimiter = table.remove(list, #list) or " ";
	return table.concat(list, delimiter);
end



--============================================================================--
-- Arithmatic
--============================================================================--

--- Determines if a given value lies within the range constructed by 'bottom' and 'top'
-- @param value (number) Value to check
-- @param bottom (number) Lower bound of the range
-- @param top (number) Upper bound of the range
-- @return (boolean) True if 'value' is in the range; false otherwise
function lib.inRange(value, bottom, top)
	return lib.isNumber(value) and (bottom <= value) and (value <= top);
end



--============================================================================--
-- Frames
--============================================================================--

--- Sets a given frame's point in screen coordinates. All existing points on the frame will be cleared in the process.
-- @param frame (Frame) The frame on which the point will be set
-- @param left (number) Distance from the left edge of the screen to the left edge of the frame
-- @param top (number) Distance from the top edge of the screen to the top edge of the frame
function lib.setScreenPoint(frame, left, top)
	frame:ClearAllPoints();
	frame:SetPoint("TOPLEFT", UIParent, "TOPLEFT", left, -top);
end



--============================================================================--
-- Type Queries
--============================================================================--

--- Determines if a given value is a string.
-- @param value (*) The value in question
-- @return (boolean) true if the given value is a string; false otherwise
function lib.isString(value)
	return type(value) == 'string';
end

--- Determines if a given value is a number.
-- @param value (*) The value in question
-- @return (boolean) true if the given value is a number; false otherwise
function lib.isNumber(value)
	return type(value) == 'number';
end

--- Determines if a given value is a function.
-- @param value (*) The value in question
-- @return (boolean) true if the given value is a function; false otherwise
function lib.isFunction(value)
	return type(value) == 'function';
end

--- Determines if a given value is a boolean.
-- @param value (*) The value in question
-- @return (boolean) true if the given value is a boolean; false otherwise
function lib.isBoolean(value)
	return type(value) == 'boolean';
end

--- Determines if a given value is nil.
-- @param value (*) The value in question
-- @return (boolean) true if the given value is nil; false otherwise
function lib.isNil(value)
	return type(value) == 'nil';
end

--- Determines if a given value is a table.
-- @param value (*) The value in question
-- @return (boolean) true if the given value is a table; false otherwise
function lib.isTable(value)
	return type(value) == 'table';
end

--- Determines if a given value is "truthy".
-- @param value (*) The value in question
-- @return (boolean) true if the given value is "truthy"; false otherwise
function lib.isTruthy(value)
	return (value and true) or false;
end

--- Determines if a given value is "falsy".
-- @param value (*) The value in question
-- @return (boolean) true if the given value is "falsy"; false otherwise
function lib.isFalsy(value)
	return not lib.isTruthy(value);
end

--- Determines if a given value is a UI Frame.
-- @param value (*) The value in question
-- @return (boolean) true if the given value is a UI Frame; false otherwise
function lib.isFrame(value)
	local isFunction = lib.isFunction;
	return (value and isFunction(value.SetFrameStrata) and isFunction(value.SetFrameLevel))
end



--============================================================================--
-- Other
--============================================================================--

--- Standard No-op function. Does nothing.
function lib.noop() end

--- Dumps a description of a given value (and its contents, in the case of a table).
-- @param value (*) Some value to dump
-- @return (*) The result of the global DevTools_Dump() function
function lib.dump(value)
	return DevTools_Dump(value);
end

--- Wraps a given function in a "function" that will replace itself with the no-op function after it is invoked.
-- @param func (function) Function to be wrapped
-- @return (table) A callable table that will replace its own logic with the no-op function after its first invocation
function lib.createEphemeralFunction(func)
	local metatable = {};

	-- Set __call function to execute the given function
	-- and then replace itself with the no-op function
	metatable.__call = function(...)
		func(...);
		metatable.__call = lib.noop;
	end

	-- Return a new table that will ace as the function
	return setmetatable({}, metatable);
end