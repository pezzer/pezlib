local lib = PezLib:NewLibrary("PezModuleBase");

-- If lib is nil, then the library must already be loaded
if (not lib) then
	return;
end


-- Cache globals
local LibStub = LibStub;
local format = format;
local pairs = pairs;


-- Load required libs
local PezUtil = LibStub("PezUtil");
local PezClass = LibStub("PezClass");



------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Module class definition
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

-- Create PezModule class
local lib = PezClass:Create(lib);

--- Class constructor.
-- @param name (string) The name of the module
-- @param console (table) PezConsole instance used for printing to chat
-- @param parent (table) The parent module, nil if there is no parent (an addon instance)
function lib:__constructor(name, title, console, parent)

	-- Pseudo-private fields
	self.__private = {
		initialized = false,
		parent = parent,
		modules = {},
		defaults = {},
		console = console,
		db = nil
	};

	-- Addon info
	self.name = name;
	self.title = title;
	self.identifier = parent and parent:Prefix(name) or name;

	-- DB proxies
	self.db = nil;
	self.cdb = nil;
	self.pdb = nil;

	-- Embed console functions
	console:Embed(self);
end

--- Initializes the instance. This function is called
-- automatically and should never be called explicitly.
function lib:Initialize()

	-- Short-circuit if already initialized
	if (self:IsInitialized()) then
		return
	end

	-- Initialize persistence
	self:InitializeDB();

	-- Set initialized flag
	self.__private.initialized = true;
	self:Debug('Initialized');

	-- Call 'OnInitialize()'
	self:OnInitialize();

	-- Initialize sub-modules
	for _, module in self:IterateModules() do
		module:Initialize();
	end
end



--============================================================================--
-- Module functions
--============================================================================--

--- Creates a child module.
-- @param name (string) The name of the module
-- @return (table) The module created
function lib:NewModule(name)
	local PezModule = LibStub("PezModule");

	-- Get handle to modules table
	local modules = self.__private.modules;

	-- Verify a module with the given name doesn't already exist
	if (modules[name]) then
		return self:Throwf("A module with the name '%s' already exists!", self.title);
	end

	-- Instantiate module
	local module = PezModule:New(name, self);

	-- Store created module in module list
	modules[name] = module;

	-- Return the module
	return module;
end

--- Returns an iterator for looping through the module's sub-modules.
-- @return (*) Pairs of name (string) and module (table) for iterating
function lib:IterateModules()
	return pairs(self.__private.modules);
end

--- Retrieve a module by name.
-- @param name (string) The name of a module
-- @return (table) The module matching the given name, or nil if none was found
function lib:GetModule(name)
	return self.__private.modules[name];
end



--============================================================================--
-- DB functions
--============================================================================--

--- Sets the defaults to be used during DB initialization.
-- @param module (table) The module being configured
-- @param defaults (table) Set of defaults to be used
-- @param dataType (string) AceDB data type for which the defaults should be used
local function setDefaults(module, defaults, dataType)

	-- Can't set defaults after the module has been initialized
	if (module:IsInitialized()) then
		module:Error("Defaults must be set before a module is initialized");
	end

	-- Store defaults to be used during DB initialization
	module.__private.defaults[dataType] = defaults;
end

--- Sets global DB defaults.
-- @param defaults (table) Default settings for the global DB
function lib:SetDefaults(defaults)
	setDefaults(self, defaults, 'global');
end

--- Sets character DB defaults.
-- @param defaults (table) Default settings for the character DB
function lib:SetCharacterDefaults(defaults)
	setDefaults(self, defaults, 'char');
end

--- Sets profile DB defualts.
-- @param defaults (table) Default settings for the profile DB
function lib:SetProfileDefaults(defaults)
	setDefaults(self, defaults, 'profile');
end



--============================================================================--
-- Other functions
--============================================================================--

--- Prefixes a given string with the name of the addon. Example: 'MyFrame' --> 'AddonName_ModuleName_MyFrame'.
-- @param str (string) String to prefix
-- @return (string) Prefixed string
function lib:Prefix(str)
	return format("%s_%s", self.identifier, str);
end

--- Determines whether the module has been initialized.
-- @return (boolean) True if the ADDON_LOADED event has fired for the associated addon and initialization
-- of the module is complete; false otherwise
function lib:IsInitialized()
	return self.__private.initialized;
end



--============================================================================--
-- Template methods
--============================================================================--

lib.InitializeDB = PezUtil.noop;
lib.OnInitialize = PezUtil.noop;