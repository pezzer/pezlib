local lib = PezLib:NewLibrary("PezAddon");

-- If lib is nil, then the library must already be loaded
if (not lib) then
	return;
end


-- Cache globals
local _G = _G;
local LibStub = LibStub;
local GetAddOnInfo = GetAddOnInfo;
local GetAddOnMetadata = GetAddOnMetadata;
local select = select;
local table = table;


-- Load required libs
local AceDB = LibStub("AceDB-3.0");
local PezUtil = LibStub("PezUtil");
local PezModuleBase = LibStub("PezModuleBase");
local PezCommand = LibStub("PezCommand");
local PezDB = LibStub("PezDB");
local PezConsole = LibStub("PezConsole");


-- Define local constants
local UNIVERSAL_DEFAULTS = {
	global = {
		debug = false
	}
};


------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Addon class definition
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

-- Create class
local lib = PezModuleBase:Extend(lib);

--- Class constructor.
-- @param name (string) The name of the addon
function lib:__constructor(name)

	-- Get title from toc
	local title = select(2, GetAddOnInfo(name));

	-- Create console for printing
	local console = PezConsole:New(self);


	-- Call superclass constructor
	PezModuleBase.__constructor(self, name, title, console);

	-- Initialize fields
	self.__private.console = PezConsole:New(self);
	self.__private.components = {};
	self.version = GetAddOnMetadata(name, "Version");
end

--- Initializes an AceDB instance for a given addon and sets up proxies for accessing
-- the global (db), per-character (cdb) and profile (pdb) DBs.
function lib:InitializeDB()

	-- Assume the name of the saved variables file is '<addon name>DB', and get the saved variables
	local svName = self.name .. 'DB';
	local savedVariables = _G[svName];

	-- Verify saveed variables were found
	if (not savedVariables) then
		return self:Throwf("No saved variables found for %s. Saved variables should be configure as '%sDB' in %s.toc", addon.title, addon.name, addon.name);
	end


	-- Configure an AceDB instance
	local db = AceDB:New(savedVariables, self.__private.defaults, true);

	-- Create DB proxies
	self.db = PezDB:CreateProxy(db, "global");
	self.cdb = PezDB:CreateProxy(db, "char");
	self.pdb = PezDB:CreateProxy(db, "profile");

	-- Store saved variables and DB instance in addon's private data for later reference
	self.__private.db = db;
end



--============================================================================--
-- Component functions
--============================================================================--

--- Returns a list of components added to the addon.
-- @return (table) List of addon components
function lib:GetComponents()
	return self.__private.components;
end

--- Adds a component to the addon that will be included in the return from PezAddon:Unpack().
-- This is a convenient way to provide access to data that will be needed across multiple
-- source files.
-- @param ... (*) Components to be included in the addon
function lib:AddComponent(...)
	PezUtil.append(self.__private.components, ...);
end



--============================================================================--
-- Overrides
--============================================================================--

--- Sets global DB defaults. This overrides a superclass function in order to merge universal defaults.
-- @param defaults (table) Default settings for the global DB
function lib:SetDefaults(defaults)

	-- Merge configured defaults with universal defaults
	defaults = PezUtil.merge(table.copy(UNIVERSAL_DEFAULTS), defaults);

	-- Call superclass implementation
	PezModuleBase.SetDefaults(self, defaults);
end



--============================================================================--
-- Commands
--============================================================================--

--- Registers a root-level console command.
-- @param name (string) The name of the command (without '/')
-- @param description (string) An optional description of the command's action
-- @param action (function) Action to be performed (if no sub-commands are provided for execution)
-- @return (table) The created command
function lib:RegisterCommand(name, description, action)

	-- If a root command has already been registered, there's nothing to do
	if (self.__private.command) then
		return;
	end

	-- Create and store the command
	self.__private.command = PezCommand:New(self, name, description, action);

	-- Return the command to facilitate chaining
	return self.__private.command;
end

---
-- Returns the addon's root console command.
-- @return (table) The addon's root console command instance
function lib:GetCommand()
	return self.__private.command;
end



--============================================================================--
-- Other functions
--============================================================================--

--- Returns whether the addon has been loaded
-- @return (boolean) True if the addon has been loaded; false otherwise
function lib:IsLoaded()
	return self.__private.loaded;
end



--============================================================================--
-- Template methods
--============================================================================--

lib.OnLogin = PezUtil.noop;