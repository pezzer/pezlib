local lib = PezLib:NewLibrary("PezDB");

-- If lib is nil, then the library must already be loaded
if (not lib) then
	return;
end


-- Cache globals
local LibStub = LibStub;
local setmetatable = setmetatable;


-- Load required libs
local PezClass = LibStub("PezClass");


------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Module class definition
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

-- Create class
lib = PezClass:Create(lib);


--- Creates a proxy to an AceDB instance that will provide access to a particular data type.
-- @param db (table) AceDB instance to proxy
-- @param dataType (string) AceDB data type specifying which specific table within the
-- AceDB instance the proxy should point to
function lib:CreateProxy(db, dataType)
	return setmetatable({}, {
		__index = function(tbl, key)
			return db[dataType][key];
		end,
		__newindex = function(tbl, key, value)
			db[dataType][key] = value;
		end
	});
end