local lib = PezLib:NewLibrary("PezCommand");

-- If lib is nil, then the library must already be loaded
if (not lib) then
	return;
end


-- Cache globals
local LibStub = LibStub;
local format = format;
local select = select;
local pairs = pairs;
local rawget = rawget;
local unpack = unpack;
local table = table;


-- Load required libs
local AceConsole = LibStub("AceConsole-3.0");
local PezUtil = LibStub("PezUtil");
local PezClass = LibStub("PezClass");
local PezColor = LibStub("PezColor");


-- Define local constants
local COMMAND_COLOR = PezColor.QUALITY.LEGENDARY;
local DESCRIPTION_COLOR = PezColor.QUALITY.POOR;



--============================================================================--
-- Local functions
--============================================================================--

--- Registers a given command via AceConsole.
-- @param command (table) The command to register
local function registerCommand(command)
	local name = command:GetName();

	-- Register chat command via AceConsole
	AceConsole:RegisterChatCommand(name, function(msg)

		-- Convert message into argument table
		local args = PezUtil.split(msg);

		-- Execute root command
		command(unpack(args));
	end);
end

--- Adds a sub-command to the given command that will toggle debug mode for its associated addon.
-- @param command (table) Command for which the sub-command will be created
local function addDebugCommand(command)
	local addon = rawget(command, "addon");

	-- Add sub-command that toggles debug mode
	command:AddCommand("debug", "Toggle debug mode", function()

		-- Toggle debug mode
		addon.db.debug = not addon.db.debug;

		-- Print state change message
		local enabled = PezColor.ON("enabled");
		local disabled = PezColor.OFF("disabled");
		addon:Print("Debug mode " .. (addon.db.debug and enabled or disabled));
	end);
end

--- Constructs a list of command and sub-command descriptions, recursively, for all commands stemming from
-- the given command.
-- @param command (table) A command instance
-- @param prefix (string) Command string of the parent command - used in recursion
-- @return (table) List of command description strings
local function getCommandDescriptions(command, prefix)
	local name = command:GetName();
	local description = command:GetDescription();
	local action = rawget(command, "action");
	local subCommands = rawget(command, "subCommands");

	-- Construct command string
	-- If no prefix, it must be the root command
	local commandString = prefix and format("%s %s", prefix, name) or ("    /" .. name);


	-- Create table to hold descriptions
	local commandDescriptions = {};

	-- If action exists, add command description for current command
	if (action) then
		local commandDescription = COMMAND_COLOR(commandString);
		if (description) then
			commandDescription = commandDescription .. " - " .. DESCRIPTION_COLOR(description);
		end
		PezUtil.append(commandDescriptions, commandDescription);
	end

	-- For each sub-command,
	-- Get list of command descriptions and append it
	for _, subCommand in pairs(subCommands) do
		local subCommandDescriptions = getCommandDescriptions(subCommand, commandString);
		PezUtil.appendAll(commandDescriptions, subCommandDescriptions);
	end

	-- Return list of command descriptions
	return commandDescriptions;
end

--- Lists all supported chat commands related to a given command's command tree.
-- @param command (table) An command instance
local function listAvailableCommands(command)
	local addon = rawget(command, "addon");

	-- Find root command
	local parent = rawget(command, "parent");
	while (parent) do
		command = parent;
		parent = rawget(command, "parent");
	end

	-- Construct table of command description lines
	local descriptionsTable = getCommandDescriptions(command);

	-- Prepend header
	PezUtil.prepend(descriptionsTable, "Available commands:");

	-- Print descriptions
	addon:Print(table.concat(descriptionsTable, "|n"))
end


------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Command class definition
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

-- Create class
local lib = PezClass:Create(lib);

--- Class constructor. If no parent command is provided, the created command will be registered as
-- a chat command.
-- @param addon (table) Addon associated with the command (used for print
-- @param name (string) The name of the command
-- @param description (string) An optional description of the command's action
-- @param action (function) Action to be performed (if no sub-commands are provided for execution)
-- @param parent (table) Optional parent command
function lib:__constructor(addon, name, description, action, parent)

	-- Reassign field values if no description provided
	if (PezUtil.isFunction(description)) then
		parent = action;
		action = description;
		description = nil;
	end


	-- Initialize fields
	self.addon = addon;
	self.parent = parent;
	self.name = name;
	self.action = action;
	self.description = description;
	self.subCommands = {};

	-- If a root command register an chat command and add the debug sub-command
	if (not parent) then
		registerCommand(self);
		addDebugCommand(self);
	end
end

--- Determines what value should be returned on an access query. This function will return a class member matching
-- the provided key, if one exists; otherwise, it will search for a sub-command with matching name.
-- @param tbl (table) Table being accessed
-- @param key (string) Key with which the table is being queried
-- @return (*)
function lib:__index(tbl, key)
	return lib[key] or rawget(self, "subCommands")[key];
end

--- Executes the command.
-- @param ... (string) List of arguments passed to the command
-- @return (*) Any value returned from the command's action function
function lib:__call(...)

	-- Check first argument for sub-command
	-- If found, execute the sub-command with all but the first argument
	local subCommand = rawget(self, "subCommands")[...];
	if (subCommand and lib:IsInstance(subCommand)) then
		return subCommand(select(2, ...));
	end

	-- No sub-command found, so execute the action or default action
	local action = rawget(self, "action") or listAvailableCommands;
	return action(self, ...);
end



--============================================================================--
-- Accessors
--============================================================================--

--- Returns the name of the command.
-- @return (string) The name of the command
function lib:GetName()
	return rawget(self, "name");
end

--- Returns the command's description.
-- @return (string) The command's description
function lib:GetDescription()
	return rawget(self, "description");
end



--============================================================================--
-- Sub-commands
--============================================================================--

--- Creates a new command that is a sub-command of this command.
-- @param name (string) The name of the command
-- @param description (string) An optional description of the command's action
-- @param action (function) Action to be performed (if no sub-commands are provided for execution)
-- @return (table) The created command
function lib:AddCommand(name, description, action)
	local addon = rawget(self, "addon");
	local subCommands = rawget(self, "subCommands");

	-- Reassign field values if no description provided
	if (PezUtil.isFunction(description)) then
		action = description;
		description = nil;
	end

	-- Verify name doesn't conflict with existing commands
	if (subCommands[name]) then
		return addon:Throwf("A command with the name '%s' already exists", name);
	end

	-- Verify name doesn't conflict with any method names
	if (lib[name]) then
		addon:Throwf("The name '%s' is used internally, and is therefor restricted from use as a command name", name);
	end


	-- Create and store the command
	subCommands[name] = lib:New(addon, name, description, action, self);

	-- Return the command
	return subCommands[name];
end