local NAME = "PezLib";
local VERSION = 1;


-- Create library via LibStub
local lib = LibStub:NewLibrary(NAME, VERSION);

-- If lib is nil, then the library must already be loaded
if (not lib) then
	return;
end


-- Store lib name/version info
lib.name = NAME
lib.version = VERSION;

-- Store lib globally
_G[NAME] = lib;


-- Cache globals
local LibStub = LibStub;
local unpack = unpack;
local error = error;
local CreateFrame = CreateFrame;



------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Library definition
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

-- List of created PezAddons
local addons = {};



--============================================================================--
-- Local functions
--============================================================================--

--- Verify the given arguments are the source file arguments. Unfortunately, we can't do much other than verifying
-- a Sequence of string, table, and nil arguments.
-- @param ... (*) List of what is presumed to be the source file arguments
-- @return (string) Addon name as supplied in source file arguemnts
-- @return (table) Root addon table as supplied in source file arguments
local function verifySourceFileArguments(...)
	local PezUtil = lib.PezUtil;

	-- Grab name, namespace, and following argument (presumably nil)
	local name, root, next = ...;

	-- Verify source file arguments
	if (not PezUtil.isString(name) or not PezUtil.isTable(root) or next) then
		return error("Function expects to be passed the source file arguemnts. Use: '<Function>(...)'");
	end

	-- Return source file arguments
	return name, root;
end

--- Enables all of a given addon/module's sub-modules.
-- @param module (table) PezModule or PezAddon instance
local function enableModules(module)

	-- For each module...
	for _, submodule in module:IterateModules() do

		-- Set enabled state to the default
		submodule:SetEnabledState(submodule.__private.defaultEnabledState);

		-- Recursively enable/disable all submodules
		enableModules(submodule);
	end
end



--============================================================================--
-- Event handling
--============================================================================--

-- Listen for events indicating the game has loaded, and initialize/enable addons.
local eventFrame = CreateFrame("Frame");
eventFrame:RegisterEvent("ADDON_LOADED");
eventFrame:RegisterEvent("PLAYER_LOGIN");
eventFrame:SetScript("OnEvent", function(eventFrame, event, arg1)

	-- If a PezAddon was loaded, initialize it
	if (event == "ADDON_LOADED" and addons[arg1]) then
		addons[arg1]:Initialize();
	end

	if (event == "PLAYER_LOGIN") then
		lib.PezUtil.map(addons, function(addon)
			addon:OnLogin();
			enableModules(addon);
		end);
	end
end);



--============================================================================--
-- Library functions
--============================================================================--

--- Retrieves a PezAddon instance by name.
-- @param name (string) The name of the addon
-- @return (table) The PezAddon instance associated with the given name
function lib:GetAddon(name)
	return addons[name];
end

--- Pulls pertinent addon components out of the root addon table. This function is designed to
-- be passed the argument list passed to addon files. A PezAddon instance will be created on first
-- invocation.
-- @param ... (*) Source file arguments
-- @return (table) PezAddon instance
-- @return (table) PezUtil util function library
-- @return (table) PezColor class
-- @return (*) List of components added to the PezAddon instance
function lib:GetAddonComponents(...)
	local PezAddon = self.PezAddon;
	local PezUtil = self.PezUtil;
	local PezColor = self.PezColor;

	-- Verify passed values are source file arguments, and grab addon name
	local name = verifySourceFileArguments(...);

	-- Initialize addon, if needed
	if (not PezAddon:IsInstance(addons[name], true)) then
		addons[name] = PezAddon:New(name);
	end

	-- Return addon components
	return addons[name], PezUtil, PezColor, unpack(addons[name]:GetComponents());
end

--- Creates a new library via LibStub with the same version number as the PezLib library. The created library is
-- attached to the PezLib library instance prior to return. Optionally, the key on which the library is attached
-- can be specified.
-- @param name (string) Name of the library (passed to LibStub)
-- @param key  (string) Key used to attach the created library to the PezLib library instance
-- @return (table) The created library
function lib:NewLibrary(name, key)

	-- Create library
	local lib = LibStub:NewLibrary(name, self.version);

	-- If lib is nil, then the library must have already been loaded
	if (not lib) then
		return;
	end

	-- Store lib name/version info
	lib.name = name;
	lib.version = self.version;

	-- Store in parent library for convenience
	self[key or name] = lib;

	-- Return the libarary
	return lib;
end