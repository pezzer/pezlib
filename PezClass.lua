local lib = PezLib:NewLibrary("PezClass");

-- If lib is nil, then the library must already be loaded
if (not lib) then
	return;
end


-- Cache globals
local LibStub = LibStub;
local getmetatable = getmetatable;
local setmetatable = setmetatable;
local tContains = tContains;
local pairs = pairs;


-- Load required libs
local PezUtil = LibStub("PezUtil");


------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Base class definition
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

-- Create base class
local BaseClass = {};

--- Instantiates an instance of the class. If a '__constructor()' method is defined on the class, it will be called
-- on the instance with any additional parameters passed. Subclasses will be responsible to call the __constructor()
-- function explicitly.
-- @param ... (*) List of arguments to be passed to the class constructor
-- @return (table) Class instance
function BaseClass:New(...)

	-- Instanticate the class
	local instance = lib:Extend(self, {}, PezUtil.keyset(BaseClass));

	-- Call constructor if defined
	if (PezUtil.isFunction(self.__constructor)) then
		self.__constructor(instance, ...);
	end

	-- Store reference to class for convenience
	instance.__class = getmetatable(instance);

	-- Return instance
	return instance;
end

--- Creates a class that is a subclass of this class. Optionally a table can be provided to be "class-ified" into
-- the subclass.
-- @param tbl (table) An optional table to be "class-ified".
-- @return (table) The subclass created
function BaseClass:Extend(tbl)

	-- Classify given table
	local class = lib:Classify(tbl);

	-- Make the class a extend from this class
	class = lib:Extend(self, lib:Create(class));

	-- Store reference to parent class for convenience
	class.__parent = self;

	return class;
end

--- Determines if the given object is an instance of the class. If the 'direct' flag is set to 'true', the function
-- will only check if the object is a direct child of the class, without traversing the rest of the hierarchy.
-- @param object (table) A possible child of the class
-- @param direct (boolean) True to only check for direct inheritance, without traversing the inheritance hierarchy
-- @return (boolean) True if the given object is an instance of the class
function BaseClass:IsInstance(object, direct)

	-- If no object/class provided, there's nothing to do
	if (not object) then
		return false;
	end

	-- If the object's class reference is this class, then it is an instance
	if (object.__class == self) then
		return true;
	end

	-- If direct is true, we're done
	if (direct) then
		return false;
	end

	-- Try the parent class
	return self.__parent and self.__parent:IsInstance(object);
end



------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Lib functions
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

--- Creates a new class, embedding a 'New' method in a given table, allowing it to be instantiated.
-- @param class (table) An optional table to be "class-ified".
-- @return (table) The class table (same as 'class' if provided)
function lib:Create(class)

	-- Initialize a class table if none was provided
	class = class or {};

	self:Extend(BaseClass, class);

	-- Return the class table for chaining
	return class;
end

--- Alias to 'Create()'
lib.Classify = lib.Create;

--- Establishes a parent/child relationship between two tables. The parent table will
-- be set as the index of the child's metatable, so the child will "extend" from the parent.
-- If no child table is provided, a one will be created.
-- @param parent (table) Parent table
-- @param child (table) Child table
-- @param prohibited (table) List of members to prohibit access to (used for class instantiation)
function lib:Extend(parent, child, prohibited)

	-- Initialize an object if none was provided
	child = child or {};

	-- Set the provided class' __index field to a function that will
	-- search the parent class for any members not explicitly prohibited
	parent.__index = function(tbl, key)
		if (prohibited and tContains(prohibited, key)) then
			return;
		end
		return parent[key];
	end;

	-- Set the metatable of the object to make it an instance of the given class
	setmetatable(child, parent);

	-- Return the object, which is now an instance of 'class'
	return child;
end

--- Determines if a given value is a class. For efficiency's sake, this function simply checks if the given value
-- is a table with a 'New' and 'Extend' member that match the base class functions.
-- @param value (*) The value in question
-- @return (boolean) True if the given value is a subclass of the PezClass base class; flase otherwise
function lib:IsClass(value)

	-- Verify given value is a table
	if (not PezUtil.isTable(value)) then
		return false;
	end

	-- Verify that each function of the base class is shared with the given table
	for key in pairs(BaseClass) do
		if (PezUtil.isFunction(BaseClass[key]) and BaseClass[key] ~= value[key]) then
			return false;
		end
	end

	-- At this point, the value must be a subclass of the base class
	return true;
end