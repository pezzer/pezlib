local lib = PezLib:NewLibrary("PezModule");

-- If lib is nil, then the library must already be loaded
if (not lib) then
	return;
end


-- Cache globals
local LibStub = LibStub;
local pairs = pairs;


-- Load required libs
local PezUtil = LibStub("PezUtil");
local PezModuleBase = LibStub("PezModuleBase");
local PezColor = LibStub("PezColor");
local PezDB = LibStub("PezDB");
local PezConsole = LibStub("PezConsole");


------------------------------------------------------------------------------------------------------------------------
--====================================================================================================================--
-- Module class definition
--====================================================================================================================--
------------------------------------------------------------------------------------------------------------------------

-- Create PezModule class
local lib = PezModuleBase:Extend(lib);

--- Class constructor.
-- @param name (string) The name of the module
-- @param parent (table) The parent module, nil if there is no parent (an addon instance)
function lib:__constructor(name, parent)

	-- Create module title by colorizing the module name
	local title = PezColor.MODULE(name);

	-- Create console for printing
	local addon = parent.GetAddon and parent:GetAddon() or parent;
	local console = PezConsole:New(addon, title);


	-- Call superclass constructor
	PezModuleBase.__constructor(self, name, title, console, parent);

	-- Initialize enabled state
	self.__private.defaultEnabledState = true;
	self.__private.enabled = false;
end

--- Initializes the module's DB references.
function lib:InitializeDB()
	local addon = self:GetAddon();

	-- Create AceDB instance as a namepaces within the addon's DB
	local db = addon.__private.db:RegisterNamespace(self.identifier, self.__private.defaults);

	-- Create DB proxies
	self.db = PezDB:CreateProxy(db, 'global');
	self.cdb = PezDB:CreateProxy(db, 'char');
	self.pdb = PezDB:CreateProxy(db, 'profile');

	-- Store AceDB instance in module's private data for later reference
	self.__private.db = db;
end



--============================================================================--
-- Enable/Disable
--============================================================================--

---
-- Returns whether the module is enabled. This will always return false prior to the PLAYER_LOGIN event.
-- @return (boolean) True if the module is enabled; false otherwise
function lib:IsEnabled()
	return self.__private.enabled;
end

--- Sets whether the module should be enabled or disabled upon initialization.
-- @param enabled (boolean) True to enabled the module upon initialization; false to leave it disabled
function lib:SetDefaultEnabledState(enabled)
	self.__private.defaultEnabledState = enabled;
end

---
-- Enables or disables the module according to a provided flag.
-- @param enable (boolean) True to enable the module; false to disable it
function lib:SetEnabledState(enabled)
	enabled = PezUtil.isTruthy(enabled);

	-- If the state hasn't changed, there's nothing to do
	if (self.__private.enabled == enabled) then
		return;
	end


	-- Update enabled state
	self.__private.enabled = enabled;

	-- Call appropiate template method
	if (enabled) then
		self:OnEnable();
	else
		self:OnDisable();
	end
end

--- Enables the module.
function lib:Enable()
	self:SetEnabledState(true);
end

--- Disables the module.
function lib:Disable()
	self:SetEnabledState(false);
end


--============================================================================--
-- Print functions (generated aliases to addon print functions)
--============================================================================--

--- Constructs a wrapper to one of the associated addon's print functions.
-- @param name (string) Name of the print function to wrap
local function wrapPrintFunc(name)
	return function(module, ...)
		local addon = module:GetAddon();
		return addon[name](addon, ...);
	end
end

-- Generate print functions
for _, funcName in pairs({ "Print", "Printf", "Debug", "Debugf", "Error", "Errorf", "Throw", "Throwf" }) do
	lib[funcName] = wrapPrintFunc(funcName);
end



--============================================================================--
-- Other functions
--============================================================================--

--- Returns the parent module/addon of this module
-- @return (table) The parent of the module, which can be either an addon instance or another module
function lib:GetParent()
	return self.__private.parent;
end

--- Returns the addon this module is a part of.
-- @return (table) The PezAddon instance associated with this module
function lib:GetAddon()
	local parent = self:GetParent();
	return lib:IsInstance(parent) and parent:GetAddon() or parent;
end



--============================================================================--
-- Template methods
--============================================================================--

lib.OnEnable = PezUtil.noop;
lib.OnDisable = PezUtil.noop;